---
#- name: git checkout dspace
#  sudo: false
#  git:
#    repo="{{ application_repo }}"
#    dest="{{ application_src }}"
#    accept_hostkey=true
#    version='tags/dspace-5.4'

  - name: ensure postgresql is running
    action: service name=postgresql state=started

  - name: ensure tomcat is running
    action: service name=tomcat state=started

  - name: install ImageMagick and Ghostscript
    yum: name={{ item }} state=latest
    with_items:
      - ghostscript
      - ImageMagick

  - name: Add vagrant user to tomcat group
    user: name=vagrant groups=tomcat append=yes

  - name: Give tomcat group write permission to {{ application_home }}
    file: path={{ application_home }} state=directory mode="g+w" recurse=yes

  - name: Install sass 
    gem: name=sass version=3.3.14 state=present user_install=no

  - name: Install compass
    gem: name=compass user_install=no state=present

  - name: Download and unpack dspace src release.
    unarchive: src=https://github.com/DSpace/DSpace/releases/download/dspace-5.5/dspace-5.5-src-release.tar.gz dest=/home/vagrant dest=/tmp copy=no owner=tomcat

  - name: copy initial dspace configuration to source files
    template: src=build.properties.j2 dest="{{ application_src }}/build.properties"

  - name: kludge. Make embargo/ dir
    file: path="{{ application_src }}/dspace/modules/additions/src/main/java/org/dspace/embargo" state=directory

  - name: kludge. Copy custom embargo java
    become: yes
    copy: src="/vagrant/files/dspace/modules/additions/src/main/java/org/dspace/embargo/UwEmbargoSetter.java"  dest="{{ application_src }}/dspace/modules/additions/src/main/java/org/dspace/embargo/UwEmbargoSetter.java"  owner=tomcat

  - name: kludge. Make statistics/ dir
    file: path="{{ application_src }}/dspace/modules/additions/src/main/java/org/dspace/statistics" state=directory

  - name: kludge. Copy custom solr statistics java
    become: yes
    copy: src="/vagrant/files/dspace/modules/additions/src/main/java/org/dspace/statistics/SolrLogger.java"  dest="{{ application_src }}/dspace/modules/additions/src/main/java/org/dspace/statistics/SolrLogger.java"  owner=tomcat

  - name: initial install mvn package
    become: yes
    become_user: tomcat
    command: "mvn package -Dmirage2.on=true chdir={{ application_src }} creates={{ application_src }}/dspace/target"
    #command: "mvn package -Dmirage2.on=true -Dmirage2.deps.included=false chdir={{ application_src }} creates={{ application_src }}/dspace/target"
    environment:
      GEM_HOME: /home/vagrant/.gem/ruby/gems
      GEM_PATH: /home/vagrant/.gem/ruby/gems/:/usr/share/rubygems

  - name: Create application directory
    file: path={{ application_home }} state=directory owner=tomcat group=tomcat mode=0777

  - name: initial install ant install
    become: yes
    become_user: tomcat
    command: "ant fresh_install chdir={{ application_src }}/dspace/target/dspace-installer creates={{ application_home }}/webapps"
    when: initial_install is not defined
    register: initial_install

  - name: ant update
    become: yes
    become_user: tomcat
    command: "ant update chdir={{ application_src }}/dspace/target/dspace-installer"
    when: initial_install is defined

  - name: install general dspace configuration
    template: src=dspace.cfg.j2 dest="{{ application_home }}/config/dspace.cfg"

  - name: install oai configuration
    template: src=oai.cfg.j2 dest="{{ application_home }}/config/modules/oai.cfg"

  - name: install solr-statistics.cfg
    template: src=solr-statistics.cfg.j2 dest="{{ application_home }}/config/modules/solr-statistics.cfg"

  - name: kludge to deal with multiple repository dependencies
    become: yes
    copy: src="/vagrant/files/uwlib-ansible-files/config/{{ item }}"  dest="{{ application_home }}/config/{{ item }}"  owner=tomcat
    with_items:
      - xmlui.xconf
      - input-forms.xml
      - item-submission.xml 

  - name: kludge to deal with multiple repository dependencies 2
    become: yes
    copy: src="/vagrant/files/uwlib-ansible-files/webapps/xmlui/{{ item }}"  dest="{{ application_home }}/webapps/xmlui/{{ item }}" owner=tomcat
    with_items:
      - i18n/messages.xml 
      - 'themes/Mirage2/xsl/core/page-structure.xsl'
      - 'themes/Mirage2/images/uw-letter-logo.png'
      - 'themes/Mirage2/styles/main.css'

  - name: symlink dspace webapps to tomcat webapps dir
    file: src="{{ application_home }}/webapps/{{ item.src }}" dest="/var/lib/tomcat/webapps/{{ item.dest }}" state=link
    with_items:
      - { src: 'oai', dest: 'oai' }
      - { src: 'rdf', dest: 'rdf' }
      - { src: 'rest', dest: 'rest' }
      - { src: 'solr', dest: 'solr' }
      - { src: 'sword', dest: 'sword' }
      - { src: 'swordv2', dest: 'swordv2' }
      - { src: 'xmlui', dest: 'xmlui' } 
    notify: restart tomcat

  - name: create dspace administrator account
    command: "{{ application_home }}/bin/dspace create-administrator -e {{ dspace_admin_email }} -f {{ dspace_admin_firstname }} -l {{ dspace_admin_lastname }} -p {{ dspace_admin_pwd }} -c {{ dspace_admin_language }}" 
