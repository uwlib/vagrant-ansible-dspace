# vagrant-ansible-dspace   
This repository should build a development version of dspaceRW v.5.5

## Prerequisites:
 - Vagrant
 - Virtualbox

## Check if centos/7 image is already available
`vagrant box list`

## Install centos 7 virtualbox image if needed
`vagrant box add centos/7 https://atlas.hashicorp.com/centos/boxes/7`

## Check that it has installed
`vagrant box list`

## Clone this repo
`git clone git@bitbucket.org:uwlib/vagrant-ansible-dspace.git`

## Clone researchworks repo
`git clone git@bitbucket.org:uwlib/researchworks.git`

## Clone uwlib-ansible repo
`git clone git@bitbucket.org:uwlib/uwlib-ansible.git`

## Create a vagrant-ansible-dspace/files/ directory
`mkdir vagrant-ansible-dspace/files`

## make $SOMEWHERE equal to the root of the codebase
`SOMEWHERE=$PWD`

## cd into vagrant-ansible-dspace/files and create symlinks
`cd vagrant-ansible-dspace/files`   

`ln -s $SOMEWHERE/researchworks/dspace`   

`ln -s $SOMEWHERE/researchworks/dspace-xmlui`

`ln -s $SOMEWHERE/uwlib-ansible/roles/researchworks/files uwlib-ansible-files`   

`ln -s $SOMEWHERE/uwlib-ansible/roles/researchworks/templates uwlib-ansible-templates` 

## cd into vagrant-ansible-dspace
`cd $SOMEWHERE/vagrant-ansible-dspace/`

## Copy and edit vars.yml.template   
`cp vars.yml.template vars.yml`

## Copy and edit private.yml.template
`cp private.yml.template private.yml`

## Edit private.yml if you want to use different values for the database's name, user, password, etc.

## Start your vagrant box
`vagrant up --provider virtualbox`

## ssh into vagrant box
`vagrant ssh`

## cd into sync/ dir and run ansible playbook
`cd /vagrant`   
`ansible-playbook -i inventory playbook.yml` This can be run many times.   

## test to see that site is running
`curl localhost:8080/xmlui/`   
then, open a browser and test to see that it works outside the VM by going to [localhost:8080/xmlui/](http://localhost:8080/xmlui/)   
If it does not work in the browser try ssh-ing back into the VM and stopping firewalld   
`sudo systemctl stop firewalld`

## Create a new dspace admin
`sudo /dspace/bin/dspace create-administrator`

and follow the prompts

## To make changes
Change files on the host machine (not the vm)
`vagrant rsync` on the host machine to update files to the vm.

## To get RW's metadata (this is the gist of it)
 - ask Ian or Mike to get you a copy of RW's postgresql db. Ask if it's a text or binary dump.
 - copy that dump into $SOMEWHERE/vagrant-ansible-dspace
 - `vagrant rsync`
 - `vagrant ssh`
 - `cd sync`
 - Stop tomcat in case it is running: `sudo systemctl stop tomcat`
 - Drop the existing DSpace db.   
   `sudo -u postgres psql`   
   `DROP DATABASE [dbname];`  
   Exit out of psql: `\q`
 - Recreate the database:  
   `sudo -u postgres createdb -O [dbuser] [dbname]`
 - If the dump is a binary file, use the command:  
   `pg_restore -h 127.0.0.1 -O -U [dbuser] -d [dbname] [filename]`  
   If it's a text dump:  
   Change the previous dbuser name (usually dspace_researchworks2 to dbuser:  
   `sed -i 's/dspace_researchworks2/[dbuser]/g' [dumpfile.sql]`  
   Load the new database:  
   `psql -f [dumpfile.sql] -U [dbuser] -d [dbname] -h127.0.0.1 -W`   
   Enter [dbpasswd] on prompt
 - Restart tomcat: `sudo systemctl start tomcat`
 - Re-index dspace (this will take a while):  
   `sudo -u tomcat /dspace/bin/dspace index-discovery`


## Running Tomcat

- To check if tomcat is running:  
  `sudo systemctl status tomcat`

- To start tomcat (takes a bit):  
  `sudo systemctl start tomcat`
  


----------
## Useful vagrant/virtualbox things to know ##
 - Default url is http://www.vagrantbox.es/, so you can just add what you find there to your Vagrantfile and say 'vagrant up' and skip the vagrant init NAME step

### vagrant commands ####
  - $ vagrant box list   (see all VMs that have been downloaded)
  - $ vagrant provision   (update a box that is already *up*)
  - $ vagrant ssh  (enter into the box that is *up*)
  - $ vagrant status  (what boxes are currently doing)
  - $ vagrant halt (stops vm)
  - $ vagrant destroy (stops vm and destroys all of the resources it created while running *as though it never existed in the first place*)
  - $ vagrant box remove NAME  
--If a box has multiple providers, the exact provider must be specified with the --provider flag.   
--If a box has multiple versions, you can select what versions to delete with the --box-version flag.
- for more: https://docs.vagrantup.com/v2/cli/index.html
